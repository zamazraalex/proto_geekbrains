//
//  Credential.swift
//  Proto
//
//  Created by Алексей Рябов on 08.09.2018.
//  Copyright © 2018 Zamazra games. All rights reserved.
//

import Foundation


/// Инкапсулирует логику хранения авторизационных данных пользователя.
public class Credential {
    
    public static let sharedInstance: Credential = Credential()
    public static let tokenKey = "Credential.tokenKey"
    
    
    /// Показывает зарегистрирован ли пользователь на текущий момент.
    public var isAuthorized: Bool {
        get{
            guard let _ = self.token else {
                return false
            }
            return true
        }
    }
    
    /// Хранит ссесионный ключ пользователя.
    /// (возвращает nil в случае его отсутствия)
    public var token: String? {
        get {
            return UserDefaults.standard.value(forKey: Credential.tokenKey) as? String
        }
        set{
            UserDefaults.standard.set(newValue, forKey: Credential.tokenKey)
        }
    }
}

